﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tienda2 : MonoBehaviour
{//ACTIVAR TEXT Y FUNCION SEGUN EL TAG DE LA TIENDA
    public GameObject questionVel;
    public GameObject questionVida;
    public GameObject questionRec;
    public GameObject questionDis;
    public GameObject Player;

	GameObject curQuestion;

	public int mejora;
    int monedas;

	public int precio;

    // Start is called before the first frame update
    void Start()
    {
        questionDis.SetActive(false);
        questionRec.SetActive(false);
        questionVel.SetActive(false);
        questionVida.SetActive(false);
		switch (mejora)
		{
			case 0:
				curQuestion = questionVida;
				
				break;

			case 1:
				curQuestion = questionVel;
				break;

			case 2:
				curQuestion = questionRec;
				break;

			case 3:
				curQuestion = questionDis;
				break;
		}
	}

    // Update is called once per frame
    void Update()
    {
		if(Player != null)
		{
			Player.GetComponent<PlayerController>().puntos = monedas;
		}
        
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            Player = other.gameObject;
            monedas = Player.GetComponent<PlayerController>().puntos;
            
            //questionRec.SetActive(true);
            //questionVel.SetActive(true);
            //questionVida.SetActive(true);

            
        }
    }

    public void OnTriggerStay(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
			curQuestion.SetActive(true);

            if (Input.GetKeyDown(KeyCode.E))
            {
				PlayerController statsPlayer = other.gameObject.GetComponent<PlayerController>();
                if (precio <= monedas)
                {
					switch (mejora)
					{
						case 0:
							if(statsPlayer.mejoraVida == false)
							{
								statsPlayer.MejoraVida(true);
								monedas -= precio;
							}
							
							break;

						case 1:
							if (statsPlayer.mejoraVelocidad == false)
							{
								statsPlayer.mejoraVelocidad = true;

								monedas -= precio;
							}

							break;

						case 2:
							if (statsPlayer.mejoraRecargarSpeed == false)
							{
								statsPlayer.mejoraRecargarSpeed = true;

								monedas -= precio;
							}

							break;

						case 3:
							if (statsPlayer.mejoraDisparo == false)
							{

								statsPlayer.mejoraDisparo = true;

								monedas -= precio;
							}

							break;
					}

                    //Player.GetComponent<DataUpgrade>().Recarga -= 5;

                    //Player.GetComponent<barra_vida>().puntosVidaMax += 50;
                    //Player.GetComponent<barra_vida>().puntosVida = Player.GetComponent<barra_vida>().puntosVidaMax;

                    //Player.GetComponent<DataUpgrade>().Velocidad += 5;

                    
                   
                    

                }
            }
        }
    }
	private void OnTriggerExit(Collider other)
	{
		if (other.gameObject.CompareTag("Player"))
		{
			curQuestion.SetActive(false);
		}
		
	}
}
