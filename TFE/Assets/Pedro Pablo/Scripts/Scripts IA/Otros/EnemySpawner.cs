﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


public class EnemySpawner : MonoBehaviour
{
	//public GameObject enemy;
	//   public GameObject flyer;
	//   public GameObject SpawnPoint;
	//public int zombie_counter;
	//public int flyer_counter;

	//public GameObject[] buscador_zombie;
	//public GameObject[] buscador_flyer;


	// Start is called before the first frame update
	void Start()
	{

	}

	// Update is called once per frame
	void Update()
	{
		if (Input.GetKeyDown(KeyCode.H))
		{
			if (BoltNetwork.IsServer)
			{
				Spawner(3, 2);
			}

		}
		//buscador_zombie = GameObject.FindGameObjectsWithTag("Enemy");
		//buscador_flyer = GameObject.FindGameObjectsWithTag("Fly");

		//if((buscador_zombie.Length == 0) && (buscador_flyer.Length == 0))
		//{
		//    Debug.Log("No Enemies");
		//    zombie_counter = zombie_counter + (zombie_counter / 4);
		//    flyer_counter = flyer_counter + (flyer_counter / 4);


		//}
	}

	public void Spawner(float hp, float speed)
	{

		GameObject newEnemy = BoltNetwork.Instantiate(BoltPrefabs.limo_skin_Variant, this.transform.position, Quaternion.identity);
		//BoltNetwork.Instantiate(BoltPrefabs.LevitatorFSM, this.transform.position, Quaternion.identity);
		newEnemy.GetComponent<Walker>().vida = hp;
		newEnemy.GetComponent<NavMeshAgent>().speed = speed;

	}

}
