﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class bullet_behaviour : MonoBehaviour
{
    public Rigidbody rb;
    //public Vector3 shoter;
    public GameObject player;
    public float force;
    public Vector3 rayDirection;
    public GameObject[] Enemy_Finder;
    public List<GameObject> visible_enemy;

    // Start is called before the first frame update
    void Start()
    {

        //player = GameObject.FindGameObjectWithTag("heart");       
        rb = GetComponent<Rigidbody>();
        Enemy_Finder = GameObject.FindGameObjectsWithTag("heart");
        BuscarEnemigo();
        rayDirection = player.transform.position - transform.position;
        rb.AddForce(rayDirection * force, ForceMode.Impulse);
        //Destroy(this.gameObject, 0.5f);

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerEnter(Collider coll)
    {
        if(coll.gameObject.tag == "Player")
        {
            //Debug.Log("hit");
            Destroy(this.gameObject, 0.5f);
        }
    }

    public void BuscarEnemigo()
    {
        visible_enemy.Clear();

        for (int i = 0; i < Enemy_Finder.Length; i++)
        {
            visible_enemy.Add(Enemy_Finder[i]);
        }

        visible_enemy = visible_enemy.OrderBy(o => Vector3.Distance(transform.position, o.transform.position)).ToList<GameObject>();

        player = visible_enemy[0].gameObject;
    }
}
