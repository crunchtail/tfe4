﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class barra_vida : MonoBehaviour
{
    public float puntosVida;
    public float puntosVidaMax;

    public Image barravida;

    // Start is called before the first frame update
    void Start()
    {
        puntosVida = puntosVidaMax;
    }

    // Update is called once per frame
    void Update()
    {
        if (puntosVida > puntosVidaMax) { puntosVida = puntosVidaMax; }

        barravida.fillAmount = puntosVida / puntosVidaMax;
    }

    private void OnTriggerEnter(Collider other)
    {
        if((other.gameObject.tag == "bullet")|| (other.gameObject.tag == "Enemy"))
        {
            puntosVida -= 5;

            if(puntosVida <= 0)
            {
                Destroy(this.gameObject);
            }
        }
    }
}
