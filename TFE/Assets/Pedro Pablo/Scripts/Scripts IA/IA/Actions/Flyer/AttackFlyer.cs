﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AttackFlyer : State
{
    public Animator anim;
    public float distanceToPlayer;
    public float minPlayerDetectDistance;
    public float AtkDistance;
    public Vector3 rayDirection;
    public bool canAtk;
    public bool IsPursuit;
    public bool IsAtk;
    public NavMeshAgent NavM;
    public Agent agente;
    public Collider coll;
    public GameObject target;
    public GameObject capsule;

    public void Update(GameObject npc)
    {
        if (canAtk)
        {
            IsAtk = true;
            IsPursuit = false;

            
        }
        else
        {
            IsAtk = false;
            IsPursuit = true;
        }

        anim.SetBool("IsPursuit", IsPursuit);
        anim.SetBool("IsAtk", IsAtk);

        npc.GetComponent<Flyer>().capsule.transform.rotation = capsule.transform.rotation;
    }

    public AttackFlyer()
    {
        stateID = StatesID.AttackFlyer;
        

    }

    public override void Reason(GameObject player, GameObject npc)
    {

        if (distanceToPlayer > AtkDistance)
        {

            //canAtk = false;

            IsAtk = false;
            IsPursuit = true;
            npc.GetComponent<Flyer>().IsAtk = IsAtk;
            npc.GetComponent<Flyer>().IsPursuit = IsPursuit;


            npc.GetComponent<Flyer>().SetTransition(TransitionID.ToChasePlayerFlyer);
        }

        if (target == null)
        {
            IsAtk = false;
            IsPursuit = true;
            npc.GetComponent<Flyer>().IsAtk = IsAtk;
            npc.GetComponent<Flyer>().IsPursuit = IsPursuit;

            npc.GetComponent<Flyer>().SetTransition(TransitionID.ToFindPlayerFlyer);
        }

        if (npc.GetComponent<Flyer>().vida <= 0)
        {
            npc.GetComponent<Flyer>().SetTransition(TransitionID.ToDieFlyer);
            npc.GetComponent<Flyer>().Die();
        }
    }

    public override void Act(GameObject player, GameObject npc)
    {
        Debug.Log("Can see player");
        capsule = npc.GetComponent<Flyer>().capsule;
        Vector3 PointAtLook = new Vector3(player.transform.position.x, player.transform.position.y, player.transform.position.z);
        capsule.transform.LookAt(PointAtLook);
        IsPursuit = true;

        AtkDistance = npc.GetComponent<Flyer>().minAtkDistance;
        target = npc.GetComponent<Flyer>().player;
        NavM = npc.GetComponent<NavMeshAgent>();
        agente = npc.GetComponent<Agent>();

        Debug.Log("AttackFlyer");

        IsAtk = true;

        npc.GetComponent<Flyer>().IsAtk = IsAtk;
        npc.GetComponent<Flyer>().IsPursuit = IsPursuit;

        minPlayerDetectDistance = npc.GetComponent<Flyer>().minPlayerDetectDistance;
        distanceToPlayer = npc.GetComponent<Flyer>().distanceToPlayer;
    }

  

}
