﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using System.Linq;

public class FindPlayerFlyer : State
{
    public GameObject[] Enemy_Finder_W;
    public List<GameObject> visible_enemy_W;
    public Agent agente;
    public GameObject target;
    public NavMeshAgent NavM;
    public float distanceToPlayer;
    public float minPlayerDetectDistance;



    public FindPlayerFlyer()
    {
        stateID = StatesID.FindPlayerFlyer;

    }

    public override void Reason(GameObject player, GameObject npc)
    {
        if (target != null)
            npc.GetComponent<Flyer>().SetTransition(TransitionID.ToChasePlayerFlyer);
    }

    public override void Act(GameObject player, GameObject npc)
    {
        Debug.Log("FindPlayerWalker");
        BuscarEnemigo(npc);

    }

    public void BuscarEnemigo(GameObject npc)
    {
        Enemy_Finder_W = GameObject.FindGameObjectsWithTag("Player");
        visible_enemy_W = new List<GameObject>();

        for (int i = 0; i < Enemy_Finder_W.Length; i++)
        {
            visible_enemy_W.Add(Enemy_Finder_W[i]);
        }

        visible_enemy_W = visible_enemy_W.OrderBy(o => Vector3.Distance(npc.transform.position, o.transform.position)).ToList<GameObject>();

        if (visible_enemy_W.Count <= 0)
        {
            return;
        }
        npc.GetComponent<Flyer>().player = visible_enemy_W[0].gameObject;

        agente.target = visible_enemy_W[0].gameObject;

        minPlayerDetectDistance = npc.GetComponent<Flyer>().minPlayerDetectDistance;
        distanceToPlayer = npc.GetComponent<Flyer>().distanceToPlayer;


        target = npc.GetComponent<Flyer>().player;
    }
}

