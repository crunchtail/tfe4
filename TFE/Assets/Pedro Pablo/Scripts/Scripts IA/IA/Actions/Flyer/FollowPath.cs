﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class FollowPath : State
{
    public Transform[] points;
    public int destPoint = 0;
    public NavMeshAgent agent;


    void Update()
    {
        // Choose the next destination point when the agent gets
        // close to the current one.
        if (!agent.pathPending && agent.remainingDistance < 0.5f)
            GotoNextPoint();
    }

    public FollowPath(GameObject npc) //Asignacion de variables
    {
        stateID = StatesID.Patrol;
        agent = npc.GetComponent<Flyer>().NavM;
        agent.autoBraking = false;       
    }


    public override void Reason(GameObject player, GameObject npc)
    {
        

    }

    public override void Act(GameObject player, GameObject npc)
    {
        GotoNextPoint();
    }


    void GotoNextPoint()
    {
        // Returns if no points have been set up
        if (points.Length == 0)
            return;

        // Set the agent to go to the currently selected destination.
        agent.destination = points[destPoint].position;

        // Choose the next point in the array as the destination,
        // cycling to the start if necessary.
        destPoint = (destPoint + 1) % points.Length;
    }


   
}

