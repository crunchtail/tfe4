﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DieWalker : State
{


    // Start is called before the first frame update
    public void Update(GameObject npc)
    {

    }

    // Update is called once per frame
    public DieWalker()
    {
        stateID = StatesID.DieWalker;


    }

    public override void Reason(GameObject player, GameObject npc)
    {
       
    }

    public override void Act(GameObject player, GameObject npc)
    {
        npc.GetComponent<Walker>().NavM.isStopped = true;
        npc.GetComponent<Walker>().agente.enabled = false;
        npc.GetComponent<Walker>().enabled = false;
        npc.GetComponent<Walker>().coll.enabled = false;
        npc.GetComponent<Walker>().coll2.enabled = false;
    }
}
