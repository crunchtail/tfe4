﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class IdleWalker : State
{
    public float distanceToPlayer;
    public float minPlayerDetectDistance;
	public float minDist;
	public NavMeshAgent NavM;
	public bool isPursuit;

	public void Update()
    {
        
    }

    public IdleWalker(GameObject npc)
    {
        stateID = StatesID.IdleWalker;
    }

    public override void Reason(GameObject player, GameObject npc)
    {
       if(distanceToPlayer <= minDist)
            npc.GetComponent<Walker>().SetTransition(TransitionID.ToAttackWalker);

        if (player == null)
            npc.GetComponent<Walker>().SetTransition(TransitionID.ToFindPlayerWalker);
   

        if (npc.GetComponent<Walker>().vida <= 0)
        {
            npc.GetComponent<Walker>().SetTransition(TransitionID.ToDieWalker);
            npc.GetComponent<Walker>().Die();
        }
    }

    public override void Act(GameObject player, GameObject npc)
    {
        Debug.Log("IdleWalker");
		//Destroy(npc);
		

		NavM = npc.GetComponent<NavMeshAgent>();
        player = npc.GetComponent<Walker>().player;
		Vector3 PointAtLook = npc.transform.position + NavM.desiredVelocity;
		npc.transform.LookAt(PointAtLook);
		minDist = NavM.stoppingDistance;

		npc.GetComponent<Walker>().minAtkDistance = minDist;
		minPlayerDetectDistance = npc.GetComponent<Walker>().minPlayerDetectDistance;
        distanceToPlayer = npc.GetComponent<Walker>().distanceToPlayer;
    }

}
