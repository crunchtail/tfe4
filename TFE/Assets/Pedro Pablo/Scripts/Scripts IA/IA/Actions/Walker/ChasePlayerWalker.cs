﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ChasePlayerWalker : State
{
    public float distanceToPlayer;
    public float minPlayerDetectDistance;
    public bool IsPursuit;
    public Animator anim;
    public NavMeshAgent NavM;
    public float MinDist;

    public void Update()
    {
        anim.SetBool("IsPursuit", IsPursuit);
    }

    public ChasePlayerWalker()
    {
        stateID = StatesID.ChasePlayerWalker;
       
    }

    public override void Reason(GameObject player, GameObject npc)
    {
        if (distanceToPlayer < MinDist)
            npc.GetComponent<Walker>().SetTransition(TransitionID.ToAttackWalker);
        else
            npc.GetComponent<Walker>().SetTransition(TransitionID.ToIdleWalker);

    }

    public override void Act(GameObject player, GameObject npc)
    {
        Debug.Log("ChasePlayerWalker");
        Vector3 PointAtLook = new Vector3(player.transform.position.x, npc.transform.position.y, player.transform.position.z);
        npc.transform.LookAt(PointAtLook);
        IsPursuit = true;
        npc.GetComponent<Walker>().IsPursuit = IsPursuit;

        NavM = npc.GetComponent<NavMeshAgent>();
        NavM.speed = 3;
        MinDist = NavM.stoppingDistance;

        npc.GetComponent<Walker>().minAtkDistance = MinDist;

        minPlayerDetectDistance = npc.GetComponent<Walker>().minPlayerDetectDistance;
        distanceToPlayer = npc.GetComponent<Walker>().distanceToPlayer;
    }

    
}
