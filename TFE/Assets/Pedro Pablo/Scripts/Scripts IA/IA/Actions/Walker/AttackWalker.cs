﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AttackWalker : State
{
    public Animator anim;
    public float distanceToPlayer;
    public float minPlayerDetectDistance;
    public float AtkDistance;
    public Vector3 rayDirection;
    public bool canAtk;
    public bool IsPursuit;
    public bool IsAtk;
    public NavMeshAgent NavM;
    public Agent agente;
    public Collider coll;
    public GameObject target;

    public void Update(GameObject npc)
    {

        anim.SetBool("IsPursuit", IsPursuit);
        anim.SetBool("IsAtk", IsAtk);
    }

    public AttackWalker()
    {
        stateID = StatesID.AttackWalker;
        

    }

    public override void Reason(GameObject player, GameObject npc)
    {
        

        if (distanceToPlayer > AtkDistance)
        {

			//canAtk = false;
			Debug.Log("Cambiar a estado perseguir");
            IsAtk = false;
            
            

            npc.GetComponent<Walker>().IsAtk = IsAtk;
			
			npc.GetComponent<Walker>().SetTransition(TransitionID.ToIdleWalker);
        }
		else
		{
			Debug.Log("seopfgr");
		}

        if(target == null)
        {
            IsAtk = false;
            
            
            npc.GetComponent<Walker>().IsAtk = IsAtk;
			
            npc.GetComponent<Walker>().SetTransition(TransitionID.ToFindPlayerWalker);
        }

        if (npc.GetComponent<Walker>().vida <= 0)
        {
            npc.GetComponent<Walker>().SetTransition(TransitionID.ToDieWalker);
            npc.GetComponent<Walker>().Die();
        }

    }

    public override void Act(GameObject player, GameObject npc)
    {
		
			AtkDistance = npc.GetComponent<Walker>().minAtkDistance;
			target = npc.GetComponent<Walker>().player;
			NavM = npc.GetComponent<NavMeshAgent>();
			agente = npc.GetComponent<Agent>();

		Vector3 PointAtLook = player.transform.position;
		npc.transform.LookAt(PointAtLook);
		Debug.Log("AttackWalker");
			//Debug.Log(distanceToPlayer);

			//canAtk = true;

			IsAtk = true;

			

			npc.GetComponent<Walker>().IsAtk = IsAtk;

		

		minPlayerDetectDistance = npc.GetComponent<Walker>().minPlayerDetectDistance;
			distanceToPlayer = npc.GetComponent<Walker>().distanceToPlayer;
		
    }


}
