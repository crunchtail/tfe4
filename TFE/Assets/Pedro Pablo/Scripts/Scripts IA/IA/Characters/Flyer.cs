﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Flyer : Bolt.EntityBehaviour<IFlyerFSM>
{
    #region public variables
    public Animator anim;
    public GameObject player;
    public GameObject bullet;
    public Transform spawner;
    public float vida;
    public Transform[] path;
    public NavMeshAgent NavM;
    public Agent agente;
    public GameObject npc;
    public GameObject capsule;
    public float distanceToPlayer;
    public float minPlayerDetectDistance;
    public float minAtkDistance;
    public Vector3 rayDirection;
    public bool canAtk;
    public bool IsDying;
    public GameObject[] Enemy_Finder;
    public List<GameObject> visible_enemy;
    public bool IsAtk;
    public bool IsPursuit;
    public LayerMask PlayerLayer;

    #endregion

    #region private variables
    private StateMachine stateMachine;
    #endregion


    #region methods
    public void SetTransition(TransitionID t)
    {
        stateMachine.PerformTransition(t);
    }

    private void MakeFSM()
    {
        //if (!entity.IsOwner)
        //{
        //    state.IsAttack = anim.GetBool("IsAtk");
        //    state.IsPursuit = anim.GetBool("IsPursuit");
        //    state.isDead = anim.GetBool("IsDead");
        //}
        //if (!BoltNetwork.IsServer) return;


        FollowPath stateFollowPath = new FollowPath(npc);
        stateFollowPath.AddTransition(TransitionID.ToChasePlayerFlyer, StatesID.ChasePlayerFlyer);

        ChasePlayerFlyer stateChasePlayer = new ChasePlayerFlyer();
        stateChasePlayer.AddTransition(TransitionID.ToAttackFlyer, StatesID.AttackFlyer);
        stateChasePlayer.AddTransition(TransitionID.ToDieFlyer, StatesID.DieFlyer);

        FindPlayerFlyer stateFindPlayer = new FindPlayerFlyer();
        stateFindPlayer.agente = agente;
        stateFindPlayer.AddTransition(TransitionID.ToChasePlayerFlyer, StatesID.ChasePlayerFlyer);

        AttackFlyer stateAttackFlyer = new AttackFlyer();
        stateAttackFlyer.AddTransition(TransitionID.ToChasePlayerFlyer, StatesID.ChasePlayerFlyer);
        stateAttackFlyer.AddTransition(TransitionID.ToFindPlayerFlyer, StatesID.FindPlayerFlyer);
        stateAttackFlyer.AddTransition(TransitionID.ToDieFlyer, StatesID.DieFlyer);

        DieFlyer stateDieFlyer = new DieFlyer();


        stateMachine = new StateMachine();
        stateMachine.AddState(stateFindPlayer);
        stateMachine.AddState(stateFollowPath);
        stateMachine.AddState(stateChasePlayer);
        stateMachine.AddState(stateAttackFlyer);
        stateMachine.AddState(stateDieFlyer);
        

    }


    void Start()
    {
        NavM = GetComponent<NavMeshAgent>();
        npc = this.gameObject;
        anim = GetComponent<Animator>();
        IsAtk = false;
    }

    public override void Attached()
    {

        MakeFSM();



        state.SetTransforms(state.PosandRot, transform);
    }

    void FixedUpdate()
    {
        if (!BoltNetwork.IsServer) return;
        stateMachine.CurrentState.Reason(player, this.gameObject);
        stateMachine.CurrentState.Act(player, this.gameObject);
    }

    void Update()
    {
        if (BoltNetwork.IsServer)
        {

            if (anim.GetCurrentAnimatorStateInfo(0).IsTag("CantMove"))
            {
                NavM.isStopped = true;
            }
            else
            {
                NavM.isStopped = false;
            }

            player = PlayerMascercano();

            RaycastHit hit;
            if (player)
            {
                Vector3 rayDirection = player.transform.position - npc.transform.position;

                if (Physics.Raycast(npc.transform.position, rayDirection, out hit, Mathf.Infinity, PlayerLayer))
                { // If the enemy is very close behind the player and in view the enemy will detect the player

                    Debug.DrawRay(npc.transform.position, rayDirection, Color.red);


                    distanceToPlayer = hit.distance;
                }

                //anim.SetBool("IsPursuit", IsPursuit);
                anim.SetBool("IsAtk", IsAtk);
                anim.SetBool("IsDead", IsDying);
            }
            else
            {
                Debug.Log("NoTargetForRayCast");
            }



            state.Vida = vida;
            if (vida <= 0)
            {
                capsule.transform.LookAt(null);
                IsDying = true;
                state.isDead = IsDying;
                Debug.Log("Enviando Mensaje de Borrado");
                var msg = EnemigoMuerto.Create();
                msg.EnemyEntity = entity;
                msg.Send();

            }

            state.Vida = vida;

        }
        else
        {
            vida = state.Vida;
            anim.SetBool("IsDead", state.isDead);
        }
    }

    public void Die()
    {
        anim.SetTrigger("IsDead");
        IsDying = true;
        state.isDead = IsDying;
        Debug.Log("Enviando Mensaje de Borrado");
        var msg = EnemigoMuerto.Create();
        msg.EnemyEntity = entity;
        msg.Send();
    }

    public void Atacar()
    {
        GameObject bala = Instantiate(bullet, capsule.transform.position, Quaternion.identity);
        

    }

    public GameObject PlayerMascercano()
    {

        GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
        GameObject aux;
        aux = players[0];
        if (players.Length == 1)
        {
            return aux;
        }
        for (int i = 1; i < players.Length; i++)
        {
            if (Vector3.Distance(aux.transform.position, this.transform.position) > Vector3.Distance(players[i].transform.position, this.transform.position))
            {
                aux = players[i];
            }
        }
        return aux;


    }
    public void Damage(int cantidad)
    {
        var msg = Dañar.Create();
        msg.Vida = cantidad;
        msg.Entity = this.entity;
        msg.Send();
    }
    public void OnEvent(EnemigoMuerto evnt)
    {
        Debug.Log("Borrando enemigo de la escena");
        if (BoltNetwork.IsServer)
        {
            Debug.Log("Destruyendo enemigo desde el servidor");
            if (evnt.EnemyEntity != null)
            {
                BoltNetwork.Destroy(evnt.EnemyEntity.gameObject);
            }
            else
            {
                Debug.Log("El objeto fue destruido");
            }

        }
    }
    #endregion
}
