﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


public class Walker : Bolt.EntityBehaviour<IEnemyFSM>
{
    #region public variables
    public Animator anim;
    public float distanceToPlayer;
    public float minPlayerDetectDistance;
    public float minAtkDistance;

    public GameObject player;
    public Vector3 rayDirection;
    public bool canAtk;
    public GameObject[] Enemy_Finder;
    public List<GameObject> visible_enemy;
    public Agent agente;
    public NavMeshAgent NavM;
    public GameObject npc;
    public LayerMask playerLayer;



    public float vida;
    
    public float atk;
    public float speed;
    public Collider coll;
    public Collider coll2;


    public bool IsPursuit;
    public bool IsAtk;
    public bool IsDying;
    public int typeAtack;
    #endregion

    #region private variables
    private StateMachine stateMachine;
    #endregion


    #region methods
    public void SetTransition(TransitionID t)
    {
        stateMachine.PerformTransition(t);
    }
	
    private void MakeFSM()
    {
		////si no es el owner solo debe sincronizar las variables del animator, si es el servidor, todo lo demas
		//if (!entity.IsOwner)
		//{
		//	state.isAtack = anim.GetBool("IsAtk");
		//	state.isPursuit = anim.GetBool("IsPursuit");
  //          state.isDead = anim.GetBool("IsDead");
		//}
		//if (!BoltNetwork.IsServer) return;
        

        IdleWalker stateIdle = new IdleWalker(npc);
		stateIdle.AddTransition(TransitionID.ToAttackWalker, StatesID.AttackWalker);
        stateIdle.AddTransition(TransitionID.ToFindPlayerWalker, StatesID.FindPlayerWalker);
        stateIdle.AddTransition(TransitionID.ToDieWalker, StatesID.DieWalker);

        FindPlayerWalker stateFindPlayer = new FindPlayerWalker();
        stateFindPlayer.agente = agente;
        stateFindPlayer.AddTransition(TransitionID.ToIdleWalker, StatesID.IdleWalker);

        AttackWalker stateAttackWalker = new AttackWalker();
        stateAttackWalker.AddTransition(TransitionID.ToIdleWalker, StatesID.IdleWalker);
        stateAttackWalker.AddTransition(TransitionID.ToFindPlayerWalker, StatesID.FindPlayerWalker);
        stateAttackWalker.AddTransition(TransitionID.ToDieWalker, StatesID.DieWalker);

        DieWalker stateDieWalker = new DieWalker();


        stateMachine = new StateMachine();
        
        stateMachine.AddState(stateFindPlayer);
        stateMachine.AddState(stateDieWalker);
        stateMachine.AddState(stateIdle);
        stateMachine.AddState(stateAttackWalker);
        
    }


    void Start()
    {
		anim = GetComponent<Animator>();
		npc = this.gameObject;
        coll2 = GetComponent<Collider>();
		state.Vida = vida;
	}
	public override void Attached()
	{

		MakeFSM();
		
		
		
		state.SetTransforms(state.PosAndRoot, transform);
	}

	void FixedUpdate()
    {
		if (!BoltNetwork.IsServer) return;
        stateMachine.CurrentState.Reason(player, this.gameObject);
        stateMachine.CurrentState.Act(player, this.gameObject);

    }

    void Update()
    {
		
		//Debug.DrawLine(transform.position, player.transform.position);
		
		if (BoltNetwork.IsServer)
		{
			if (anim.GetCurrentAnimatorStateInfo(0).IsTag("CantMove"))
			{
				NavM.isStopped = true;
			}
			else
			{
				NavM.isStopped = false;
			}

			player = PlayerMascercano();

			distanceToPlayer = Vector3.Distance(this.transform.position, player.transform.position);

			//RaycastHit hit;
			if (player)
			{
				//Vector3 rayDirection = player.transform.position - npc.transform.position;

				//if (Physics.Raycast(npc.transform.position, rayDirection, out hit, Mathf.Infinity, playerLayer))
				//{ // If the enemy is very close behind the player and in view the enemy will detect the player

				Debug.DrawRay(npc.transform.position, rayDirection, Color.red);


				//    distanceToPlayer = hit.distance;
				//}

				anim.SetBool("IsPursuit", IsPursuit);
				anim.SetBool("IsAtk", IsAtk);
				



			}
			else
			{
				Debug.Log("NoTargetForRayCast");
			}


			state.Vida = vida;
			
			
		}
		else
		{
			vida = state.Vida;
			anim.SetBool("IsDead", state.isDead);
		}
		

		
    }
	
    public void Die()
    {
        anim.SetTrigger("IsDead");
        IsDying = true;
        state.isDead = IsDying;
        Debug.Log("Enviando Mensaje de Borrado");
        var msg = EnemigoMuerto.Create();
        msg.EnemyEntity = entity;
        msg.Send();
    }
    public void Ataque()
    {
        coll.enabled = true;


    }

    public void AtaqueOff()
    {
        coll.enabled = false;
    }
	public GameObject PlayerMascercano()
	{

		GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
		GameObject aux;
		aux = players[0];
		if(players.Length == 1)
		{
			return aux;
		}
		for(int i = 1; i< players.Length; i++)
		{
			if(Vector3.Distance(aux.transform.position, this.transform.position) > Vector3.Distance(players[i].transform.position, this.transform.position))
			{
				aux = players[i];
			}
		}
		return aux;


	}
	public void Damage(int cantidad)
	{
		var msg = Dañar.Create();
		msg.Vida = cantidad;
		msg.Entity = this.entity;
		msg.Send();
	}
	
	

	#endregion
}
