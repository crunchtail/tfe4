﻿#region Enumerations
public enum StatesID
{
    NullState = 0,
    FindPlayerFlyer = 1,
    FindPlayerWalker = 2,
    ChasePlayerFlyer = 3,
    ChasePlayerWalker = 4,
    Patrol = 5,
    AttackFlyer = 6,
    AttackWalker = 7,
    IdleWalker = 8,
    DieWalker = 9,
    DieFlyer = 10
}

public enum TransitionID
{
    NullTransition = 0,
    ToFindPlayerFlyer = 1,
    ToFindPlayerWalker = 2,
    ToChasePlayerFlyer = 3,
    ToChasePlayerWalker = 4,
    ToPatrol = 5,
    ToAttackFlyer = 6,
    ToAttackWalker = 7,
    ToIdleWalker = 8,
    ToDieWalker = 9,
    ToDieFlyer = 10
}
#endregion
