﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StatMenu : MonoBehaviour
{
	public float puntosVida;
	public float puntosVidaMax;

	

	public float puntosVel;
	public float puntosVelMax;

	public Image barravida;
	public Image barravel;
	

	// Start is called before the first frame update
	void Start()
	{

	}

	// Update is called once per frame
	void Update()
	{
		barravida.fillAmount = puntosVida / puntosVidaMax;
		
		barravel.fillAmount = puntosVel / puntosVelMax;
	}
}
