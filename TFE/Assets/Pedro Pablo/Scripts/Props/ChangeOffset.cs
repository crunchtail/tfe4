﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeOffset : MonoBehaviour
{

	public float scrollSpeed = 0.01f;
	public float scrollSpeed2 = 0.02f;

	Renderer rend;

	void Start()
	{
		rend = GetComponent<Renderer> ();
	}

	void Update()
	{
		float offset = Time.time * scrollSpeed;
		float offset2 = Time.time * scrollSpeed2;
		rend.material.SetTextureOffset("_MainTex", new Vector2(0, offset));
		rend.material.SetTextureOffset("_DetailAlbedoMap", new Vector2(0, offset2));
	}
}

