﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAt : MonoBehaviour {

	public Transform puntoDeMira;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		transform.LookAt(new Vector3(puntoDeMira.transform.position.x, this.transform.position.y,puntoDeMira.transform.position.z));
	}
}
