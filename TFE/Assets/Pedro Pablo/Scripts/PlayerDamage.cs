﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDamage : Bolt.EntityBehaviour<IPlayer>
{
    public PlayerController player;
    // Start is called before the first frame update
    void Start()
    {
        player = GetComponent<PlayerController>();
    }

    // Update is called once per frame
    void Update()
    {
        //if (player.vida <= 0)
        //{
        //    BoltNetwork.Destroy(this.gameObject);
        //}
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Enemy"))
        {
            player.ModificarVida(-10);
        }
    }
}
