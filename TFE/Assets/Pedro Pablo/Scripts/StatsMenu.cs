﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StatsMenu : MonoBehaviour
{
    public float puntosVida;
    public float puntosVidaMax;

    public float puntosDisparo;
    public float puntosDisparoMax;

    public float puntosRecarga;
    public float puntosRecargaMax;

    public float puntosVel;
    public float puntosVelMax;

    public Image barravida;
    public Image barravel;
    public Image barrarec;
    public Image barradis;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        barravida.fillAmount = puntosVida / puntosVidaMax;
        barradis.fillAmount = puntosDisparo / puntosDisparoMax;
        barrarec.fillAmount = puntosRecarga / puntosRecargaMax;
        barravel.fillAmount = puntosVel / puntosVelMax;
    }
}
