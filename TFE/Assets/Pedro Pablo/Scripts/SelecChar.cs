﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SelecChar : MonoBehaviour
{

    //public GameObject right, left, current;
    public GameObject ruleta;
    public Vector3 targetRotation;
    public Vector3 curRotation;
    Vector3 velocity = Vector3.zero;

    public Material unselected;
    public Material selected;

    //Array de scriptable Objects --> A traves del SO, se accede al prefab a instanciar
    //public Heroe[] Heroes;
    public GameObject names;
    public GameObject[] HeroImage;

    public int currentHeroIndex; //Heroe seleccionado

    public bool CanPress;
    public bool OneIsSelected;
    public float velocidadGiro;

	public static int PersonajeSeleccionado;

	public GameObject lobbyManager;

	public Animator[] charactersAnim;

    // Use this for initialization
    void Start()
    {
        currentHeroIndex = 0;
        CanPress = true;
        OneIsSelected = false;
        curRotation = new Vector3(0, ruleta.transform.localEulerAngles.y, 0);
        targetRotation = transform.localEulerAngles;
        UpdateHero();
		
        

    }

    // Update is called once per frame
    void Update()
    {

        if ((Input.GetKeyDown(KeyCode.A)) && (CanPress))
        {
            CanPress = false;
            turnLeft();
            if (targetRotation.y == 180)
            {
                targetRotation.y = 0;
            }
            else
            {
                targetRotation = new Vector3(0, targetRotation.y + 180, 0);
            }

            //curRotation = targetRotation;
        }
        else if ((Input.GetKeyDown(KeyCode.D)) && (CanPress))
        {
            CanPress = false;
            turnRight();
            if (targetRotation.y == 0)
            {
                targetRotation.y = 180;
            }
            else
            {
                targetRotation = new Vector3(0, targetRotation.y - 180, 0);
            }



            //curRotation = targetRotation;

        }

        else
        {
            CanPress = true;
        }
        if (transform.localEulerAngles.y == targetRotation.y)
        {
            curRotation = targetRotation;

        }
        else
        {
            transform.localEulerAngles = Vector3.SmoothDamp(transform.localEulerAngles, targetRotation, ref velocity, velocidadGiro * Time.deltaTime);
        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            OneIsSelected = true;

            if (OneIsSelected)
            {
				PersonajeSeleccionado = currentHeroIndex;

				// ir al canvas de crear partida
				lobbyManager.SetActive(true);
			}
		}

        

    }

    public void FixedUpdate()
    {
        Vector3 pointToLookAt = new Vector3(ruleta.transform.position.x, ruleta.transform.position.y, ruleta.transform.position.z);

        if (pointToLookAt != ruleta.transform.position)
            ruleta.transform.rotation = Quaternion.Slerp(ruleta.transform.rotation, Quaternion.LookRotation(pointToLookAt - ruleta.transform.position, Vector3.up), 1f);
    }


    public void turnLeft()
    {
        currentHeroIndex = leftIndex();
        UpdateHero();

    }

    public void turnRight()
    {
        currentHeroIndex = rightIndex();

        UpdateHero();
    }



    public int rightIndex()
    {
        return (currentHeroIndex == HeroImage.Length - 1) ? 0 : currentHeroIndex + 1;

    }

    public int leftIndex()
    {
        return (currentHeroIndex == 0) ? HeroImage.Length - 1 : currentHeroIndex - 1;
    }

    public void UpdateHero()
    {
        //for (int i = 0; i < HeroImage.Length; i++)
        //{
        //    HeroImage[i].GetComponent<Renderer>().material = unselected;
        //    //names[i].SetActive(false);

        //}

        //HeroImage[currentHeroIndex].GetComponent<Renderer>().material = selected;
        //names[currentHeroIndex].SetActive(true);

        //Le pasamos el objeto seleccionado al objeto persistente
        //HeroesInstanceManager.instance.playerOne = Heroes[currentHeroIndex].Model;

        names.GetComponent<StatMenu>().puntosVida = HeroImage[currentHeroIndex].GetComponent<PlayerSelectionStats>().vida;
        ////names.GetComponent<StatsMenu>().puntosDisparo = HeroImage[currentHeroIndex].GetComponent<DataUpgrade>().Disparo;
        ////names.GetComponent<StatsMenu>().puntosRecarga = HeroImage[currentHeroIndex].GetComponent<DataUpgrade>().Recarga;
        names.GetComponent<StatMenu>().puntosVel = HeroImage[currentHeroIndex].GetComponent<PlayerSelectionStats>().velocidad;
    }


}
