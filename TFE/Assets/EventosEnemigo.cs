﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventosEnemigo : Bolt.GlobalEventListener
{
	
    // Start is called before the first frame update
    void Start()
    {
		
    }
	

    // Update is called once per frame
    void Update()
    {
        
    }
	public override void OnEvent(EnemigoMuerto evnt)
	{
		Debug.Log("Borrando enemigo de la escena");
		if (BoltNetwork.IsServer)
		{
			Debug.Log("Destruyendo enemigo desde el servidor");
			if (evnt.EnemyEntity == this.GetComponent<BoltEntity>())
			{
		
				Invoke("Destroy", 3);
			}
			

		}
	}
	public override void OnEvent(Dañar evnt)
	{

		if (BoltNetwork.IsServer)
		{
			Debug.Log("Restar vida");

			if (evnt.Entity == this.GetComponent<BoltEntity>())
			{
				Debug.Log("Enemigo golpeado por " + evnt.Vida);
				if (evnt.Entity.gameObject.GetComponent<Walker>() != null)
				{
					evnt.Entity.gameObject.GetComponent<Walker>().vida -= evnt.Vida;
				}
				if (evnt.Entity.gameObject.GetComponent<Flyer>() != null)
				{
					evnt.Entity.gameObject.GetComponent<Flyer>().vida -= evnt.Vida;
				}
			}
			
				
			

			//evnt.Entity.gameObject.GetComponent<Walker>().vida -= evnt.Vida;

			

		}


		

	}
	void Destroy()
	{
		BoltNetwork.Destroy(this.gameObject);
	}
}
