﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookatScript : MonoBehaviour
{
    public Transform palmaMano;

    public Transform target;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.LookAt(target.position, palmaMano.up);
    }
}
