﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemySpawnerManager : Bolt.EntityBehaviour<IEnemySpawnManager>
{

	public EnemySpawner[] spawners;
	GameObject[] enemys;


	public float maxEnemysInScene;
	public int enemysPorRonda;
	public int enemysLeft;

	public int ronda = 1;

	float enemyHP = 1;
	float enemySpeed = 1;

	float cooldown;
	float spawnSpeed;

	float contadorZombiesPorRonda = 0;

	int numeroJugadores;

	bool inGame;
	public Text rondaText;

	// Start is called before the first frame update
	void Start()
	{
		rondaText = GameObject.FindGameObjectWithTag("RondaText").GetComponent<Text>();
	}
	public override void Attached()
	{
		if (!BoltNetwork.IsServer) return;
		ActualizarSpawns();
		StartCoroutine(IniciarPartida());


	}

	// Update is called once per frame
	void Update()
	{

		if (!inGame) return;
		rondaText.text = "Rounda " + ronda.ToString();
		if (Input.GetKeyDown(KeyCode.G))
		{
			Debug.Log("Estamos en la ronda " + ronda + " quedan " + ((int)enemysPorRonda - (int)contadorZombiesPorRonda) + " Por instanciar" + "Y el spawnSpeed es = " + spawnSpeed);
		}
		if (!BoltNetwork.IsServer) return;
		if (shouldSpawn())
		{
			if (cooldown > spawnSpeed)
			{
				instanciarEnemigo();
				cooldown = 0;
			}
			else
			{
				cooldown += Time.deltaTime;
			}
		}
		if (contadorZombiesPorRonda >= enemysPorRonda && EnemigosEnEscena() == 0)
		{
			if (!IsInvoking("CambiarRonda"))
			{
				Invoke("CambiarRonda", 10);
			}

		}


	}
	public void ActualizarSpawns()
	{
		spawners = FindObjectsOfType<EnemySpawner>();
	}
	void instanciarEnemigo()
	{

		int rand = Random.Range(0, spawners.Length);
		spawners[rand].Spawner(enemyHP, enemySpeed);
		contadorZombiesPorRonda++;


	}
	public bool shouldSpawn()
	{
		if (!inGame) return false;
		if (EnemigosEnEscena() >= maxEnemysInScene)
		{
			return false;
		}
		if (contadorZombiesPorRonda >= enemysPorRonda)
		{
			return false;
		}

		return true;
	}
	public int EnemigosEnEscena()
	{
		enemys = GameObject.FindGameObjectsWithTag("Enemy");
		return enemys.Length;
	}
	IEnumerator IniciarPartida()
	{
		yield return new WaitForSeconds(5);
		maxEnemysInScene = 25;
		numeroJugadores = FindObjectOfType<PhotonLoader>().JugadoresEnPartida();
		ronda = 1;
		enemysPorRonda = 8;
		spawnSpeed = 4;
		enemysLeft = enemysPorRonda;
		inGame = true;
	}
	public void CambiarRonda()
	{
		contadorZombiesPorRonda = 0;
		ronda++;
		state.Ronda = ronda;
		enemysPorRonda = 8 + (ronda * numeroJugadores) + ((numeroJugadores - 1) * 3);
		spawnSpeed = 4 - ronda * 0.3f;
		enemysLeft = enemysPorRonda;
		enemyHP++;
		enemySpeed += 0.2f;

	}
	public void restarEnemigos()
	{
		enemysLeft--;
	}

}
