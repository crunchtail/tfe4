﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PhotonLoader : Bolt.GlobalEventListener
{
	[SerializeField]
	public PlayerController[] players;

	BoltEntity jugador;
	BoltEntity inventario;
	BoltEntity servidor;

	spawnPointsManager spawns;

	public bool isReady;

	ControlRotCamera rotCamera;

	FollowTargetSmooth targetCamara;

	int nJugadores;

	int dictionaryIndex = 0;
	PlayerController[] jugadores = new PlayerController[4];

	public GameObject canvasLoader;

	public Transform[] puertas;

	
	public override void SceneLoadLocalDone(string scene)
	{

		Debug.Log("Diego:escena cargada en red");

		if (BoltNetwork.IsServer)
		{
			//instancia los objetos del escenario que sean online
			IniciarPartida();



		}
		//sea servidor o no se incia el cliente, para crear su propio player
		StartCoroutine(iniciarClienteLocal());



	}
	public override void SceneLoadRemoteDone(BoltConnection connection)
	{

		if (BoltNetwork.IsServer)
		{


			base.SceneLoadRemoteDone(connection);
			//crear inventario para el jugador conectado


		}



	}
	// Update is called once per frame
	void Update()
	{
		if (Input.GetKeyDown(KeyCode.K))
		{
			Debug.Log(nJugadores);
		}
	}

	private void DebugInfoPhoton()
	{
		Debug.Log("Network Status: Is server: " + BoltNetwork.IsServer);
		Debug.Log("Network Status: Is client: " + BoltNetwork.IsClient);

	}
	public void IniciarPartida()
	{

		DebugInfoPhoton();



		servidor = BoltNetwork.Instantiate(BoltPrefabs.Gamemanager, Vector3.zero, Quaternion.identity);



		BoltNetwork.Instantiate(BoltPrefabs.Pocion_recogible, Vector3.zero, Quaternion.identity);
		BoltNetwork.Instantiate(BoltPrefabs.Pocion_recogible, Vector3.zero + Vector3.right * 2, Quaternion.identity);
		BoltNetwork.Instantiate(BoltPrefabs.Pocion_recogible, Vector3.zero + Vector3.right * 4, Quaternion.identity);
		BoltNetwork.Instantiate(BoltPrefabs.m4a1Recogible, Vector3.zero - Vector3.right * 2, Quaternion.identity);
		BoltNetwork.Instantiate(BoltPrefabs.m4a1Recogible, Vector3.zero - Vector3.right * 4, Quaternion.identity);
		BoltNetwork.Instantiate(BoltPrefabs.granada_Recolectable, Vector3.zero - Vector3.right * 5 + Vector3.up * 0.5f, Quaternion.identity);
		BoltNetwork.Instantiate(BoltPrefabs.granada_Recolectable, Vector3.zero - Vector3.right * 6 + Vector3.up * 0.5f, Quaternion.identity);
		BoltNetwork.Instantiate(BoltPrefabs.granada_Recolectable, Vector3.zero - Vector3.right * 7 + Vector3.up * 0.5f, Quaternion.identity);
		BoltNetwork.Instantiate(BoltPrefabs.granada_Recolectable, Vector3.zero - Vector3.right * 8 + Vector3.up * 0.5f, Quaternion.identity);
		BoltNetwork.Instantiate(BoltPrefabs.granada_Recolectable, Vector3.zero - Vector3.right * 9 + Vector3.up * 0.5f, Quaternion.identity);


		#region instanciacion puertas
		//DoorBehaviour puerta1 = BoltNetwork.Instantiate(BoltPrefabs.Puerta, puertas[0].position, Quaternion.identity).GetComponent<DoorBehaviour>();
		//puerta1.precio = 500;

		//DoorBehaviour puerta2 = BoltNetwork.Instantiate(BoltPrefabs.Puerta, puertas[1].position, Quaternion.identity).GetComponent<DoorBehaviour>();
		//puerta2.precio = 750;

		//DoorBehaviour puerta3 = BoltNetwork.Instantiate(BoltPrefabs.Puerta, puertas[2].position, Quaternion.identity).GetComponent<DoorBehaviour>();
		//puerta3.precio = 1000;

		//DoorBehaviour puerta4 = BoltNetwork.Instantiate(BoltPrefabs.Puerta, puertas[3].position, Quaternion.identity).GetComponent<DoorBehaviour>();
		//puerta4.precio = 1000;

		//DoorBehaviour puerta5 = BoltNetwork.Instantiate(BoltPrefabs.Puerta, puertas[4].position, Quaternion.identity).GetComponent<DoorBehaviour>();
		//puerta5.precio = 1250;

		//DoorBehaviour puerta6 = BoltNetwork.Instantiate(BoltPrefabs.Puerta, puertas[5].position, Quaternion.identity).GetComponent<DoorBehaviour>();
		//puerta6.precio = 1500;

		//DoorBehaviour puerta7 = BoltNetwork.Instantiate(BoltPrefabs.Puerta, puertas[6].position, Quaternion.identity).GetComponent<DoorBehaviour>();
		//puerta7.precio = 2000;

		//DoorBehaviour puerta8 = BoltNetwork.Instantiate(BoltPrefabs.Puerta, puertas[7].position, Quaternion.identity).GetComponent<DoorBehaviour>();
		//puerta8.precio = 2000;
		#endregion
		for (int i = 0; i < 8; i++)
		{
			DoorBehaviour puerta = BoltNetwork.Instantiate(BoltPrefabs.Puerta, puertas[i].position, Quaternion.identity).GetComponent<DoorBehaviour>();
			if (i == 0)
			{
				puerta.precio = 500;
				puerta.identificador = i;

			}
			if (i >= 1 && i < 3)
			{
				puerta.identificador = i;
				puerta.precio = 1000;
				if (i == 2)
				{
					puerta.transform.eulerAngles = new Vector3(0, 90, 0);
				}
			}
			if (i >= 3 && i < 6)
			{
				puerta.identificador = i;
				puerta.precio = 1500;
			}
			if (i >= 6 && i < 9)
			{
				puerta.identificador = i;
				puerta.precio = 2000;
				if (i == 7)
				{
					puerta.transform.eulerAngles = new Vector3(0, 90, 0);
				}
			}
		}
		spawns = FindObjectOfType<spawnPointsManager>();
		Debug.Log("Diego:Iniciando manager de partida");

		BoltNetwork.Instantiate(BoltPrefabs.EnemySpawnerManager);











	}

	IEnumerator iniciarClienteLocal()
	{

		GameObject item;
		do
		{
			item = GameObject.FindGameObjectWithTag("Gamemanager");

			if (item != null) servidor = item.GetComponent<BoltEntity>();

			yield return new WaitForEndOfFrame();
		} while (servidor == null);




		//encontrar la camara en la escena
		targetCamara = FindObjectOfType<FollowTargetSmooth>();

		rotCamera = FindObjectOfType<ControlRotCamera>();

		//GUI
		InventarioGUI scriptGUI = FindObjectOfType<InventarioGUI>();
		Vector3 spawnPosition;

		while (servidor.GetState<IGameManager>().Spawns == null)
		{

			yield return new WaitForEndOfFrame();
		}
		nJugadores = servidor.GetState<IGameManager>().NumeroPlayers;
		spawnPosition = servidor.GetState<IGameManager>().Spawns[nJugadores];
		Debug.Log("Soy el jugador " + nJugadores + "  ");

		if(SelecChar.PersonajeSeleccionado == 0)
		{
			jugador = BoltNetwork.Instantiate(BoltPrefabs.Zaera, spawnPosition, Quaternion.identity);
		}
		else
		{
			jugador = BoltNetwork.Instantiate(BoltPrefabs.Player, spawnPosition, Quaternion.identity);
		}
		

		SpawnPlayer();
		canvasLoader.SetActive(false);


	}
	public void SpawnPlayer()
	{
		Debug.Log("Spawning player");
		Debug.Log(spawns);


		//for (int i = 0; i < 4; i++)
		//{
		//	if (jugadores[i] == null)
		//	{
		//		jugadores[i] = jugador.GetComponent<PlayerController>();
		//		break;
		//	}
		//}
		rotCamera.mianim = jugador.GetComponent<PlayerController>().anim;



		targetCamara.target = jugador.GetComponent<PlayerController>().targetCamara;

		var msg = AumentarJugadores.Create();

		msg.Send();
		jugador.TakeControl();
	}


	public override void OnEvent(AumentarJugadores evnt)
	{


		if (!BoltNetwork.IsServer) return;
		Debug.Log("Evento de player creado");
		servidor.GetState<IGameManager>().NumeroPlayers++;





		//servidor.GetComponent<LevelDataManager>().inventarios[servidor.GetState<IGameManager>().NumeroPlayers] = inventario.GetComponent<Inventary>();
		//for (int i = 0; i < 4; i++)
		//{
		//	if (jugadores[i] != null)
		//	{
		//		jugadores[i] = jugador.GetComponent<PlayerController>();
		//		jugadores[i].inventario = inventario.GetComponent<Inventary>();
		//		inventario.GetComponent<Inventary>().player = jugadores[i].gameObject;
		//		break;
		//	}
		//}

	}
	public int JugadoresEnPartida()
	{
		return servidor.GetState<IGameManager>().NumeroPlayers;
	}
}
