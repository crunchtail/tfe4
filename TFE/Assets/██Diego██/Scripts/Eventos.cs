﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Eventos : Bolt.GlobalEventListener
{
	public GameObject[] spawnerZone;

	public override void OnEvent(EliminarPuerta evnt)
	{
		if(evnt.identificador == 2)
		{
			spawnerZone[0].SetActive(true);
		}
		if (BoltNetwork.IsServer)
		{
			switch (evnt.identificador)
			{
				case 0:
					spawnerZone[2].SetActive(true);
					break;

				case 1:
					spawnerZone[3].SetActive(true);
					break;

				case 2:
					spawnerZone[1].SetActive(true);
					break;

				case 3:
					spawnerZone[1].SetActive(true);
					spawnerZone[5].SetActive(true);
					break;

				case 4:
					spawnerZone[6].SetActive(true);
					spawnerZone[2].SetActive(true);
					break;

				case 5:
					spawnerZone[3].SetActive(true);
					spawnerZone[4].SetActive(true);
					break;

				case 6:
					spawnerZone[6].SetActive(true);
					spawnerZone[5].SetActive(true);
					break;

				case 7:
					spawnerZone[6].SetActive(true);
					spawnerZone[4].SetActive(true);
					break;
			}
			if(evnt.entity != null)
			{
				evnt.entity.gameObject.GetComponent<DoorBehaviour>().ShowPrice(false);
				BoltNetwork.Destroy(evnt.entity.gameObject);
			}
			FindObjectOfType<EnemySpawnerManager>().ActualizarSpawns();
		}
	}
}
