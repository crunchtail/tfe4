﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arma : Objeto
{

    Animator animPersonaje;
    public GameObject particulas;
    RaycastHit hit;
    public Transform camera, mirilla, posicionBala;
    public IEnumerator coroutine;


    public int daño;
    public float cadencia;
	public float cadenciaNormal;
	public float cadenciaMejorada;

    private int balasEnElCargador;
    public int balasBolsillo;
    public int balasMaxCargador;
    public int balasMaxBolsillo;
    public bool canShoot = true;
    public bool isReloading;
	public LayerMask canHit;

    public bool LockGun;

	
  ////  public Arma cambiarArma()
  ////  {
		
  ////      LockGun = true;
		//////StartCoroutine(desbloqueoelarma(x));
		
		////return this;
  ////  }
    public int BalasEnElCargador { get => balasEnElCargador; set {
            if(value == 0)
            {
				
                Recargar();
                //InventarioGUI.singleton.ActualizaMunicion(value);
            }
            balasEnElCargador = value;
        }
    }

    // Start is called before the first frame update
    public void Start()
    {
		cadenciaNormal = cadencia;
		cadenciaMejorada = cadencia * 0.75f;
		Debug.Log("START ARMA");
        identificador = "Arma";
        canShoot = true;
		balasBolsillo = balasMaxBolsillo;
		balasEnElCargador = balasMaxCargador;
		if (camera == null)
		{
			camera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Transform>();
		}
		if (mirilla == null)
		{
			mirilla = GameObject.FindGameObjectWithTag("Mirilla").GetComponent<Transform>();
		}


	}

    // Update is called once per frame
    void Update()
    {
		if (inventario.gameObject.GetComponent<PlayerController>().mejoraDisparo)
		{
			cadencia = cadenciaMejorada;
		}
		else
		{
			cadencia = cadenciaNormal;
		}
		if (inventario.gameObject.GetComponent<PlayerController>().mejoraRecargarSpeed)
		{
			
		}

		if (BalasEnElCargador <= 0)
        {
            canShoot = false;
            if (balasBolsillo > 0)
            {
                Recargar();
            }


        }
        #region apuntado
        Ray rayo = new Ray();
        rayo.direction = camera.transform.forward;
        if (Physics.Raycast(camera.position, camera.forward, out hit, canHit, 50))
        {
            mirilla.position = hit.point;
        }
        else
        {
            mirilla.localPosition = new Vector3(0, 0, 50);
        }

        posicionBala.LookAt(mirilla);
#endregion
    }
    virtual public void Disparar()
    {
		
    }
    public bool CheckValue(int value)
    {
        if (isReloading) return false;
        if (LockGun) return false;
        if (value <= 0)
        {
            return true;
        }
        return false;
    }
    public virtual void Recargar()
    {
        //if (CheckValue(BalasEnElCargador)==true) return;
        if(balasBolsillo > 0 && balasEnElCargador < balasMaxCargador)
		{
			coroutine = RecuperarBalas();
			StartCoroutine(coroutine);
			animPersonaje.SetBool("Recargar", true);
		}
        
    }
    public void BloqueoCargador() { }
    public void SueltoCargador() { }
    public void RecargoElCargador() { }
    public override void Usar(Animator anim, PlayerController controller)
    {
        // si no tiene mu nicion retorna
        if (animPersonaje == null)
        {
            animPersonaje = anim;
        }
        if (canShoot && !isReloading && !animPersonaje.GetCurrentAnimatorStateInfo(3).IsTag("Recargar"))
        {
            anim.SetTrigger("Shoot");
            balasEnElCargador -= 1;
            canShoot = false;
            StartCoroutine(CanShootAgain());


            if (Physics.Raycast(posicionBala.position, posicionBala.forward, out hit, 50))
            {
                Idamageable idam = hit.collider.GetComponent<Idamageable>();
                if ( idam!= null)
                {
                    idam.damage(daño, hit.point);
                }
				if(hit.collider.gameObject.GetComponent<Walker>() != null)
				{
					hit.collider.gameObject.GetComponent<Walker>().Damage(daño);
					controller.ModificarPuntos(+10);
					if(hit.collider.gameObject.GetComponent<Walker>().vida <= 0)
					{
						controller.ModificarPuntos(100);
						
					}
					
				}
                if (hit.collider.gameObject.GetComponent<Flyer>() != null)
                {
                    hit.collider.gameObject.GetComponent<Flyer>().Damage(daño);
                    controller.ModificarPuntos(+10);
                    if (hit.collider.gameObject.GetComponent<Flyer>().vida <= 0)
                    {
                        controller.ModificarPuntos(100);

                    }

                }
                Debug.Log(hit.collider.gameObject);
                Instantiate(particulas, hit.point, Quaternion.identity);


            }
        }
        if(balasBolsillo > 0 && balasEnElCargador <= 0)
        {
            Debug.Log("Recargaaa");
            
            Recargar();
        }
    }
    IEnumerator CanShootAgain()
    {
        yield return new WaitForSeconds(cadencia);
        if (balasEnElCargador > 0)
        {
            canShoot = true;
        }

    }
    IEnumerator RecuperarBalas()
    {
        isReloading = true;
        yield return new WaitForSeconds(2f);
        animPersonaje.SetBool("Recargar", false);
        for (int i = 0; i < balasMaxCargador; i++)
        {
            if (balasBolsillo > 0)
            {
                if (balasEnElCargador < balasMaxCargador)
                {
                    balasEnElCargador++;
                    balasBolsillo--;
                }
            }
        }
        isReloading = false;


        canShoot = true;
        


    }


}
