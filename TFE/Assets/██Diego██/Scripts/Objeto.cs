﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum TipoObjeto {M4  = 1, pocion = 2 }
public class Objeto : MonoBehaviour
{
    public string identificador;
    public Image imagenInventario;
    public GameObject prefabParaDropear;
	public int inventaryIndex;
	public Inventary inventario;
	public int precio;
	public TipoObjeto Tipo;



    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
		
    }
    public virtual void Usar(Animator anim, PlayerController controller)
    {

    }
    public virtual void Coger()
    {
        
    }
	public void SetPartent()
	{
		transform.SetParent(inventario.gameObject.GetComponent<PlayerController>().posicionObjetos);
		transform.localPosition = Vector3.zero;
		transform.localRotation = Quaternion.identity;
	}
	#region Metodos Estaticos

	public static Objeto GetObjeto(TipoObjeto _tipo)
	{
		Objeto retorno = new Objeto();
		switch (_tipo)
		{
			case TipoObjeto.M4:
				retorno = new M4A1();

				break;
			case TipoObjeto.pocion:
				retorno = new PocionRegeneradora();

				break;
		}
		return retorno;
	}
	public int GetIndex()
	{
		int retorno = 0;
		switch (Tipo)
		{
			case TipoObjeto.M4:
				retorno = 0;

				break;
			case TipoObjeto.pocion:
				retorno = 1;

				break;
		}
		return retorno;
	}
	#endregion

}
