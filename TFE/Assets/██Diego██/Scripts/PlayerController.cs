﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

// This script moves the character controller forward
// and sideways based on the arrow keys.
// It also jumps when pressing space.
// Make sure to attach a character controller to the same game object.
// It is recommended that you make only one call to Move or SimpleMove per frame.

public class PlayerController : Bolt.EntityBehaviour<IPlayer>
{
	CharacterController characterController;
	//Estadisticas Player
	public float speed = 6.0f;
	public float speedNormal = 6;
	public float speedMejorada = 10;
	public float vida = 100;
	public float vidaMax = 100;
	public float vidaMaxMejorada = 200;
	public float vidaMaxNormal = 100;
	public int puntos = 0;

	float running = 1;
	public float jumpSpeed = 8.0f;
	public float gravity = 20.0f;
	public float rot_speed = 3;
	private Vector3 moveDirection = Vector3.zero;

	public Animator anim;
	public Transform posicionObjetos;
	public Transform lanzaGranada;
	public Transform targetCamara;


	bool jump;
	public bool isShopping;
	public bool isRunning;
	float xRoot;
	float dirX, dirZ;

	public Objeto objetoAtual;
	public Arma armaActual;
	public Consumible consumibleActual;
	public LayerMask objetosRecogibles;
	public Inventary inventario;
	public Text balasCargador, balasBolsillo, puntosText, vidaText;

	public int objetoEnMano = -1;

	public bool mejoraVida;
	public bool mejoraVelocidad;
	public bool mejoraRecargarSpeed;
	public bool mejoraDisparo;



	void Start()
	{
		objetoEnMano = -1;
		Objeto p = new Objeto();
		Arma act = new Arma();



		if (entity.IsOwner)
		{
			FindObjectOfType<InventarioGUI>().inventaryData = this.GetComponent<Inventary>();

			FindObjectOfType<AimingCamera>().posicionBala = posicionObjetos.Find("posicionApuntado");


		}
		if (p.GetType() == act.GetType())
		{
			act.Recargar();
		}
	}
	public override void SimulateOwner()
	{
		
	}
	private void Update()
	{
		#region Inputs
		//Inputs
		if (characterController.isGrounded && !isShopping)
		{
			
			//Animator
			anim.SetBool("isGrounded", true);
			anim.SetFloat("Vertical", Input.GetAxis("Vertical"));

			anim.SetFloat("Horizontal", Input.GetAxis("Horizontal"));

			//Movimiento
			dirX = Input.GetAxis("Horizontal");
			dirZ = Input.GetAxis("Vertical");

			moveDirection = new Vector3(dirX, 0.0f, dirZ);
			moveDirection = transform.TransformDirection(moveDirection);
			moveDirection = moveDirection.normalized;
			moveDirection *= speed;
			if (Input.GetButton("Fire3") && dirZ > 0)
			{
				isRunning = true;
				anim.SetBool("isRunning", true);
				//anim.SetLayerWeight(, 1);
				running = 2;
			}
			else
			{
				isRunning = false;
				anim.SetBool("isRunning", false);
				//anim.SetLayerWeight(3, 0);
				running = 1;
			}

			if (Input.GetButton("Fire2") || Input.GetKey(KeyCode.P))
			{
				Debug.Log("InputApuntar");
				if (armaActual != null)
				{
					Debug.Log("Apuntando");
					anim.SetBool("isAiming", true);
					//anim.SetLayerWeight(1, 1);
				}

			}
			else
			{
				anim.SetBool("isAiming", false);
				//anim.SetLayerWeight(1, 0);
			}
			if (Input.GetButton("Fire1"))
			{
				if (objetoAtual != null && !isRunning)
				{

					Debug.Log("Dispara");
					//anim.SetTrigger("Shoot");
					//shoot.Disparar();
					objetoAtual.Usar(anim, this);

				}
			}
			if (Input.GetKeyDown(KeyCode.R))
			{
				if (objetoAtual != null)
				{

					if (objetoAtual.identificador == "Arma")
					{
						if (armaActual != null)
						{
							armaActual.Recargar();
						}
					}
				}
			}
			if (Input.GetButtonDown("Jump")) jump = true;


		}
		else
		{
			jump = false;
			anim.SetBool("isRunning", false);
			running = 1;
			anim.SetBool("isGrounded", false);
			//anim.SetLayerWeight(1, 0);
		}
		xRoot = Input.GetAxis("Mouse X");
		if (entity.IsOwner)
		{
			state.isRunning = anim.GetBool("isRunning");
			state.isGrounded = anim.GetBool("isGrounded");
			state.Horizontal = anim.GetFloat("Horizontal");
			state.Vertical = anim.GetFloat("Vertical");
			state.GradosX = anim.GetFloat("GradosX");
		}
		

		//inventario

		if (Input.GetAxis("Mouse ScrollWheel") > 0)
		{
			inventario.CambiarObjeto(-1);
		}
		if (Input.GetAxis("Mouse ScrollWheel") < 0)
		{
			inventario.CambiarObjeto(1);
		}

		#endregion
		#region server
		//Deteccion de objetos
		if (entity.IsOwner)
		{
			Debug.DrawRay(transform.position + Vector3.up * 0.2f, transform.forward * 1f, Color.red);
			RaycastHit hit;
			if (Physics.Raycast(transform.position + Vector3.up * 0.2f, transform.forward, out hit, 1, objetosRecogibles, QueryTriggerInteraction.Collide))
			{

				if (hit.collider.gameObject.GetComponent<Recolectable>() != null)
				{
					anim.ResetTrigger("Shoot");

					if (hit.collider.gameObject.GetComponent<Recolectable>().recogido == false && !inventario.IsFull())
					{

						//GameObject nuevaArma = Instantiate(hit.collider.gameObject.GetComponent<Recolectable>().prefabRecolectable, posicionObjetos);
						Debug.Log("Instantiate arma");
						GameObject nuevaArma = Instantiate(hit.collider.gameObject.GetComponent<Recolectable>().prefabRecolectable);
						nuevaArma.transform.SetParent(posicionObjetos);
						nuevaArma.transform.localPosition = Vector3.zero;
						nuevaArma.transform.localRotation = Quaternion.identity;
						nuevaArma.GetComponent<Objeto>().inventario = inventario;


						hit.collider.gameObject.GetComponent<Recolectable>().Recoger(nuevaArma, inventario);
						hit.collider.gameObject.GetComponent<Recolectable>().DestruirObjeto();

					}




				}
			}

		}
		#endregion
		#region sync for not owners

		if (!entity.IsOwner)
		{
			if (characterController != null)
			{
				characterController.enabled = false;

			}

			anim.SetBool("isRunning", state.isRunning);
			anim.SetBool("isGrounded", state.isGrounded);
			anim.SetFloat("Horizontal", state.Horizontal);
			anim.SetFloat("GradosX", state.GradosX);
			anim.SetFloat("Vertical", state.Vertical);
			if (state.ObjetoSeleccionado != objetoEnMano)
			{


				WeaponManager managerArmas = FindObjectOfType<WeaponManager>();
				GameObject objeto = Instantiate(managerArmas.ObjetosPrefabs[state.ObjetoSeleccionado]);
				objeto.transform.SetParent(posicionObjetos);
				objeto.transform.localPosition = Vector3.zero;
				objeto.transform.localRotation = Quaternion.identity;

			}

			return;
		}
		else
		{

			state.ObjetoSeleccionado = objetoEnMano;

		}

		#endregion
		#region localData


		
		if (entity.IsOwner)
		{
			vidaText.text = vida.ToString();
			if (balasCargador == null)
			{
				balasCargador = GameObject.FindGameObjectWithTag("BalasCargador").GetComponent<Text>();
			}
			if (balasBolsillo == null)
			{
				balasBolsillo = GameObject.FindGameObjectWithTag("BalasBolsillo").GetComponent<Text>();
			}

			if (inventario.objetosInventario[inventario.index] != null)
			{
				objetoAtual = inventario.objetosInventario[inventario.index].GetComponent<Objeto>();
				if (objetoAtual.GetComponent<Arma>() != null)
				{
					armaActual = objetoAtual.GetComponent<Arma>();
				}
			}
			else
			{
				objetoAtual = null;
				armaActual = null;
				consumibleActual = null;
			}
			if (puntosText == null)
			{
				puntosText = GameObject.FindGameObjectWithTag("Puntos").GetComponent<Text>();
			}
			else
			{
				puntosText.text = puntos.ToString();

			}

			//Informacion del arma en la GUI
			if (objetoAtual != null)
			{

				if (objetoAtual.GetComponent<Arma>() != null)
				{
					armaActual = objetoAtual.GetComponent<Arma>();
					consumibleActual = null;
					balasCargador.gameObject.SetActive(true);
					balasBolsillo.gameObject.SetActive(true);

					balasBolsillo.text = objetoAtual.GetComponent<Arma>().balasBolsillo.ToString();
					balasCargador.text = objetoAtual.GetComponent<Arma>().BalasEnElCargador.ToString();
					anim.SetBool("hasWeapon", true);
				}
				if (objetoAtual.GetComponent<Consumible>() != null)
				{
					anim.SetBool("hasWeapon", false);
					consumibleActual = objetoAtual.GetComponent<Consumible>();
					armaActual = null;
				}
			}
			else
			{
				balasCargador.gameObject.SetActive(false);
				balasBolsillo.gameObject.SetActive(false);
			}
		}

		#endregion




	}

	void FixedUpdate()
	{
		if (Input.GetKeyDown(KeyCode.T))
		{
			ModificarPuntos(+5000);
		}
		if (!entity.IsOwner) return;
		if (characterController.isGrounded)
		{

			// We are grounded, so recalculate
			// move direction directly from axes           


			if (jump)
			{
				moveDirection.y = jumpSpeed;

			}
		}
		else
		{
			anim.SetBool("isGrounded", false);
		}
		//ROTACION
		transform.Rotate(0f, xRoot * rot_speed, 0);


		// Apply gravity. Gravity is multiplied by deltaTime twice (once here, and once below
		// when the moveDirection is multiplied by deltaTime). This is because gravity should be applied
		// as an acceleration (ms^-2)
		moveDirection.y -= gravity * Time.deltaTime;

		// Move the controller
		if (!isShopping)
		{

			characterController.Move(moveDirection * running * Time.deltaTime);
		}





	}
	public override void Attached()
	{

		state.SetTransforms(state.PosAndRoot, transform);
		state.SetAnimator(anim);

		if (entity.IsOwner)
		{
			inventario = GetComponent<Inventary>();

			FindObjectOfType<InventarioGUI>().inventaryData = inventario;

			inventario.OnInventaryChange += FindObjectOfType<InventarioGUI>().refresh;
		}
		characterController = GetComponent<CharacterController>();
		vidaText = GameObject.FindGameObjectWithTag("VidaText").GetComponent<Text>();

	}
	public void ModificarVida(int cantidad)
	{
		vida += cantidad;
	}
	void OnTriggerStay(Collider other)
	{
		if (!entity.IsOwner) return;
		if (other.CompareTag("Tienda"))
		{
			if (Input.GetKeyDown(KeyCode.P))
			{
				isShopping = !isShopping;
				other.GetComponent<Tienda>().AbrirTienda();
				other.GetComponent<Tienda>().ConectarConPlayer(this);
			}

		}
		if (other.GetComponent<DoorBehaviour>() != null)
		{
			DoorBehaviour puerta = other.GetComponent<DoorBehaviour>();
			puerta.ShowPrice(true);
			if (Input.GetKeyDown(KeyCode.E))
			{
				if (puerta.precio <= puntos)
				{
					puerta.OpenDoor();
					puntos -= puerta.precio;
				}
			}
		}



	}
	public void OnTriggerExit(Collider other)
	{
		if (other.GetComponent<DoorBehaviour>() != null)
		{
			Debug.Log("EsconderCartel");
			DoorBehaviour puerta = other.GetComponent<DoorBehaviour>();
			puerta.ShowPrice(false);

		}
	}
	public void ModificarPuntos(int cantidad)
	{
		puntos += cantidad;
	}
	public void ActivarControlador()
	{

	}
	public void DesactivarControlador()
	{
		isShopping = true;
		//buscocomponente camara y desactivo


	}
	public void LanzarGranada()
	{
		GameObject grenade = BoltNetwork.Instantiate(BoltPrefabs.granada);
		grenade.GetComponent<GrenadeBehaviour>().player = this;
		grenade.transform.position = lanzaGranada.position;


	}
	public void MejoraVida(bool activar)
	{
		mejoraVida = activar;
		if (activar)
		{
			vidaMax = vidaMaxMejorada;
			vida = vidaMaxMejorada;
			
		}
		else
		{
			vidaMax = vidaMaxNormal;
		}
	}
	public void MejoraRecarga(bool activar)
	{
		mejoraRecargarSpeed = activar;
	}
	public void MejoraVelocidad(bool activar)
	{
		mejoraVelocidad = activar;
	}
	public void MejoraDisparo(bool activar)
	{
		mejoraDisparo = activar;
	}





}