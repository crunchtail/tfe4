﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pila : MonoBehaviour
{
	private int cima;
	private int maximo;
	List<int> pila;
	public Pila(int max)
	{
		maximo = max;
		pila = new List<int>();
	}
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
	public bool PilaLLena()
	{
		if(cima == maximo - 1)
		{
			return true;
		}
		
		return false;
		
	}
	public bool PilaVacia()
	{
		if(cima == -1)
		{
			return true;
		}
		return false;
	}
	public void AddItem(int item)
	{
		if (!PilaLLena())
		{
			cima++;
			pila[cima] = item;
		}
		else
		{
			Debug.LogWarning("La pila esta llena");
		}
	}
	public void DeleteItem()
	{
		if (PilaVacia())
		{
			Debug.LogWarning("La pila esta vacia");
		}
		else
		{
			pila[cima] = 0;
			cima--;

		}
	}
}
