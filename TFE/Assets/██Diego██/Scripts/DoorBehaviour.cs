﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DoorBehaviour : Bolt.EntityBehaviour<IPuerta>
{
	public int precio;
	public Text PriceText;
	public int identificador;
	bool opened;
    // Start is called before the first frame update
    void Start()
    {
		if (BoltNetwork.IsServer)
		{
			state.precio = precio;
			state.identificador = identificador;
		}
		PriceText = GameObject.FindGameObjectWithTag("TextoPuertas").GetComponent<Text>();
		PriceText.text = "El precio de esta puerta es " + state.precio.ToString() + ", presiona E para abrirla";
    }

    // Update is called once per frame
    void Update()
    {
		
    }
	public void OpenDoor()
	{
		var msg = EliminarPuerta.Create();
		msg.entity = this.entity;
		msg.identificador = state.identificador;
		msg.Send();
		ShowPrice(false);
		opened = true;
		if (BoltNetwork.IsServer)
		{
			state.Open = true;
		}
		
	}
	public void ShowPrice(bool show)
	{
		if (opened)
		{
			PriceText.enabled = false;
			return;
		}
		PriceText.enabled = show;
		if (show)
		{
			PriceText.text = "El precio de esta puerta es " + state.precio.ToString() + ", presiona E para abrirla";
		}
		
		
	}
	
}
