﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSelectionStats : MonoBehaviour
{
	public float vida;
	public float velocidad;

	public GameObject prefab;
    // Start is called before the first frame update
    void Start()
    {
		vida = prefab.GetComponent<PlayerController>().vidaMax;
		velocidad = prefab.GetComponent<PlayerController>().speed;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
