﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrenadeBehaviour : MonoBehaviour
{
	public PlayerController player;
	Rigidbody rb;
	public float daño = 10;
	public float throwForce = 2;
	public float radius = 5;
	public LayerMask objetivo;
	public float force, upForce;
    public GameObject boom;
	// Start is called before the first frame update
	void Start()
    {
		rb = GetComponent<Rigidbody>();
		rb.AddForce(player.lanzaGranada.forward * throwForce, ForceMode.Impulse);
		StartCoroutine(explosion());
    }

    // Update is called once per frame
    void Update()
    {
        
    }
	private IEnumerator explosion()
	{
		yield return new WaitForSeconds(2f);

        BoltNetwork.Instantiate(boom, this.transform.position, Quaternion.identity);
		Vector3 explosionPosition = this.transform.position;
		Collider[] colliders = Physics.OverlapSphere(explosionPosition, radius, objetivo);
		foreach (Collider hit in colliders)
		{
			
			if(hit.GetComponent<Walker>() != null)
			{
				hit.GetComponent<Walker>().vida -= 10;
			}
			if (hit.GetComponent<Flyer>() != null)
			{
				hit.GetComponent<Flyer>().vida -= 10;
			}
			
			Rigidbody rg_explosion = hit.GetComponent<Rigidbody>();
			if (rg_explosion != null)
			{
				rg_explosion.AddExplosionForce(force, explosionPosition, radius, upForce, ForceMode.Impulse);
			}
		}
		BoltNetwork.Destroy(this.gameObject);
	}
}
